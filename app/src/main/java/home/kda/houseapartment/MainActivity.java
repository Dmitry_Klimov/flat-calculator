package home.kda.houseapartment;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private Button buttonFind;
    private EditText minNumberFlat;
    private EditText maxNumberFlat;
    private EditText numberFlatOnFloor;
    private EditText numberFloors;
    private EditText numberFlat;
    private TextView numberFloor;
    private CheckBox useNumberFloors;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonFind = findViewById(R.id.b_find);
        minNumberFlat = findViewById(R.id.et_min_number_flat);
        maxNumberFlat = findViewById(R.id.et_max_number_flat);
        numberFlatOnFloor = findViewById(R.id.et_number_flat_on_floor);
        numberFloors = findViewById(R.id.et_number_floors);
        numberFlat = findViewById(R.id.et_number_flat);
        numberFloor = findViewById(R.id.tv_number_floor);
        useNumberFloors = findViewById(R.id.useNumberFloors);

        useNumberFloors.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (useNumberFloors.isChecked()) {
                            numberFloors.setEnabled(true);
                        } else {
                            numberFloors.setEnabled(false);
                        }
                    }
                }
        );

        buttonFind.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String strNumberFlat = numberFlat.getText().toString();
                        String strMinNumberFlat = minNumberFlat.getText().toString();
                        String strMaxNumberFlat = maxNumberFlat.getText().toString();
                        String strNumberFlatOnFloor = numberFlatOnFloor.getText().toString();
                        String strNumberFloors = numberFloors.getText().toString();

                        if (!useNumberFloors.isChecked()) {
                            if (!strNumberFlat.equals("")
                                    && !strMinNumberFlat.equals("")
                                    && !strMaxNumberFlat.equals("")
                                    && !strNumberFlatOnFloor.equals("")
                            ) {
                                int numFlat = Integer.parseInt(strNumberFlat);
                                int minFlat = Integer.parseInt(strMinNumberFlat);
                                int maxFlat = Integer.parseInt(strMaxNumberFlat);
                                int numFlatOnFloor = Integer.parseInt(strNumberFlatOnFloor);

                                if (minFlat < maxFlat && numFlat >= minFlat) {
                                    numberFloor.setText(String.valueOf(findFloor(minFlat, maxFlat, numFlatOnFloor, numFlat)));
                                } else {
                                    Toast toastIncorrectData = Toast.makeText(getApplicationContext(),
                                            "Некорректные данные", Toast.LENGTH_SHORT);
                                    toastIncorrectData.show();
                                }
                            } else {
                                Toast toastIncorrectData = Toast.makeText(getApplicationContext(),
                                        "Некорректные данные", Toast.LENGTH_SHORT);
                                toastIncorrectData.show();
                            }
                        } else {
                            if (!strNumberFlat.equals("")
                                    && !strMinNumberFlat.equals("")
                                    && !strMaxNumberFlat.equals("")
                                    && !strNumberFloors.equals("")
                            ) {
                                int numFlat = Integer.parseInt(strNumberFlat);
                                int minFlat = Integer.parseInt(strMinNumberFlat);
                                int maxFlat = Integer.parseInt(strMaxNumberFlat);

                                if (minFlat < maxFlat && numFlat >= minFlat) {
                                    int numFloors = Integer.parseInt(strNumberFloors);
                                    int totalNumOfApartments = maxFlat - minFlat + 1;
                                    if (totalNumOfApartments % numFloors == 0) {
                                        int numFlatOnFloor = totalNumOfApartments / numFloors;
                                        numberFloor.setText(String.valueOf(findFloor(minFlat, maxFlat, numFlatOnFloor, numFlat)));
                                        numberFlatOnFloor.setText(String.valueOf(numFlatOnFloor));
                                    } else {
                                        Toast toastIncorrectData = Toast.makeText(getApplicationContext(),
                                                "Некорректное число этажей либо некорректный диапазон квартир", Toast.LENGTH_LONG);
                                        toastIncorrectData.show();
                                    }
                                } else {
                                    Toast toastIncorrectData = Toast.makeText(getApplicationContext(),
                                            "Некорректные данные", Toast.LENGTH_SHORT);
                                    toastIncorrectData.show();
                                }
                            } else {
                                Toast toastIncorrectData = Toast.makeText(getApplicationContext(),
                                        "Некорректные данные", Toast.LENGTH_SHORT);
                                toastIncorrectData.show();
                            }
                        }
                    }
                }
        );
    }

    private int findFloor(int minFlat, int maxFlat, int numFlatOnFloor, int numFlat) {
        int currNumFlat = minFlat + numFlatOnFloor - 1;
        int i = 1;
        do {
            if (currNumFlat >= numFlat) break;
            else {
                currNumFlat += numFlatOnFloor;
                i++;
            }
        } while (currNumFlat < maxFlat);
        return i;
    }
}